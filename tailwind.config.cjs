/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        gartic: {
          light: "#73b1e7",
          dark: "#0f2b44"
        }
      }
    },
  },
  plugins: [],
};
