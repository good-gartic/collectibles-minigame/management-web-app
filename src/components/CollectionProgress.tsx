import Image from "next/image";
import { type FC } from "react";
import { useQuery } from "react-query";
import { type CollectionProgressResponse } from "../types/api";

interface CollectionProgressItemProps {
    name: string,
    emoji: string,
    points: number,
    customImage: string | null,
    collected: boolean
}

const CollectionProgressItem: FC<CollectionProgressItemProps> = ({ name, emoji, points, customImage, collected }: CollectionProgressItemProps) => {
    const image = customImage !== null 
        ? <Image src={`https://i.imgur.com/${customImage}.png`} alt="Icon" width={48} height={48} />
        : <div className="text-5xl">{emoji}</div>

    const s = Math.abs(points) === 1 ? "" : "s";

    return (
        <div className={`${collected ? "bg-green-100" : "bg-gray-100"} flex flex-col py-10 px-5 items-center rounded-xl`}>
            {image}

            <div className={`${collected ? "text-green-800" : "text-gartic-dark"} font-black mt-5 mb-2 text-center`}>
                { collected && <span className="mr-2">✓</span>}
                { name }
            </div>
            <div className={`${collected ? "text-green-600" : "text-neutral-400"} font-bold`}>
                { points } point{s}
            </div>
        </div>
    );
}

const CollectionProgress: FC = () => {
    const { isLoading, data } = useQuery<CollectionProgressResponse>(
        "progress",
        () => fetch("/api/progress").then((response) => response.json())
    );

    if (isLoading) {
        return <div>Loading your collection...</div>
    }

    return (
        <div className="flex-grow grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5 gap-10">
            {
                data && data.map((item, i) =>
                    <CollectionProgressItem key={i} name={item.name} emoji={item.emoji} points={item.points} customImage={item.customImage} collected={item.collected} />
                )
            }
        </div>
    );
};

export default CollectionProgress;