import Image from "next/image";
import { useQuery } from "react-query";
import type { FC } from "react";
import type { LeaderboardResponse } from "../types/api";

type LeaderboardEntryProps = {
    place: number;
    points: number;
    name: string;
    avatar: string;
};

const LeaderboardEntry: FC<LeaderboardEntryProps> = ({ place, points, name, avatar, }: LeaderboardEntryProps) => {
    return (
        <div className="my-2 flex flex-col items-center justify-start gap-4 rounded-xl bg-white p-4 md:flex-row">
            <div className="flex flex-row items-center gap-4">
                <div className="flex-grow md:flex-grow-0 font-bold text-neutral-500">#{place}</div>
                <Image
                    src={avatar}
                    width={32}
                    height={32}
                    className="h-8 w-8 rounded-full bg-gartic-light"
                    alt={`${name}'s avatar`}
                />
                <div className="text-xl font-black text-gartic-dark">
                    {name}
                </div>
            </div>
            <div className="flex-grow text-right font-bold">{points} points</div>
        </div>
    );
};

const Leaderboard: FC = () => {
    const { isLoading, data } = useQuery<LeaderboardResponse>(
        "leaderboard",
        () => fetch("/api/leaderboard").then((response) => response.json())
    );

    if (!data || isLoading) {
        return <div>Loading the leaderboard...</div>;
    }

    return (
        <div className="flex-1 rounded-xl bg-neutral-100 p-5">
            <h1 className="text-center text-xl font-black text-gartic-dark sm:text-3xl">
                The leaderboard
            </h1>
            {data.map((entry, i) => (
                <LeaderboardEntry
                    place={i + 1}
                    name={entry.discordName}
                    avatar={entry.discordAvatar}
                    points={entry.points}
                    key={i}
                />
            ))}
        </div>
    );
};

export default Leaderboard;
