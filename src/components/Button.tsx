import type { FC, ReactNode } from "react";

interface ButtonProps {
    children?: ReactNode,
    large?: boolean,
    className?: string,
    onClick?: () => void
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
const Button: FC<ButtonProps> = ({ children, large = false, className = "", onClick = () => { } }: ButtonProps) => {
    const size = large
        ? "px-8 py-4 text-xl"
        : "px-4 py-2 text-lg";

    return (
        <button onClick={() => onClick && onClick()} className={`cursor-pointer rounded-lg bg-gartic-dark font-black text-white ${size} ${className}`}>
            {children}
        </button>
    );
};

export default Button;