import type { FC } from "react";

interface DiscordChannelProps {
    name: string
}

const DiscordChannel: FC<DiscordChannelProps> = ({ name }: DiscordChannelProps) => {
    return (
        <div className="inline-block bg-gartic-dark text-white font-bold px-2 my-1 rounded">
            <span className="text-gray-400 mr-1">#</span>{name}
        </div>
    )
};

export default DiscordChannel;