import { useSession, signOut as nextAuthSignOut } from "next-auth/react";
import Image from "next/image";
import Router from "next/router";
import Button from "./Button";

const signOut = async () => {
    await nextAuthSignOut();
    Router.push("/");
};

const Header = () => {
    const session = useSession();
    const user = session?.data?.user;

    if (!user) {
        return <header />;
    }

    return (
        <header className="flex flex-row items-center justify-between bg-gartic-light px-8 py-4">
            <div className="flex flex-row items-center bg-white p-4 pr-6 rounded-full shadow-lg">
                <Image src={user.image ?? ""} alt={`${user.name}'s avatar`} width={32} height={32} className={`rounded-full ${user.admin && "border-2 border-red-500"}`} />
                <div className="flex flex-col items-start ml-4">
                    <h1 className="text-lg tracking-tight font-bold text-gartic-dark">{user.name}</h1>
                    { user.admin && <div className="text-xs -mt-1 text-red-500 font-bold">Administrator</div>}
                </div>
            </div>

            <Button onClick={() => signOut()}>Sign out</Button>
        </header>
    );
};

export default Header;