import type { FC, ReactNode } from "react";

interface ContainerProps {
    children: ReactNode
}

const Container: FC<ContainerProps> = ({ children }) => {
    return (
        <div className="flex flex-col items-stretch md:flex-row md:items-start p-10 lg:p-20 gap-10">
            {children}
        </div>
    );
};

export default Container;