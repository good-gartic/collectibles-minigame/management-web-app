import DiscordChannel from "./DiscordChannel";
import placeholder from "../static/empty.svg";
import Image from "next/image";
import { useQuery } from "react-query";
import type { FC } from "react";
import Link from "next/link";
import Button from "./Button";

interface CollectionResponse {
    points: number;
    collection: string;
}

const EmptyCollection = () => {
    return (
        <div className="flex flex-col items-center p-5 text-center md:p-10 md:text-left">
            <Image src={placeholder} height={200} alt="" />
            <h1 className="mt-10 text-lg font-black md:text-3xl">
                You have not collected anything so far...
            </h1>
            <h2 className="mt-5 text-sm font-bold text-gartic-dark md:text-lg">
                Check the <DiscordChannel name="🎅 santapit" /> channel and wait for
                a collectible to appear!
            </h2>
        </div>
    );
};

type CollectionOverviewProps = {
    data: CollectionResponse;
}

type CollectibleItem = { type: "custom", image: string }
    | { type: "emoji", emoji: string }

const CollectionOverview: FC<CollectionOverviewProps> = ({
    data,
}: CollectionOverviewProps) => {
    const items: Array<CollectibleItem> = data.collection.split(/\s+/).map(item =>
        /%[a-zA-Z0-9]+%/.test(item) // custom image
            ? { type: "custom", image: `https://i.imgur.com/${item.replaceAll(/%/g, "")}.png` }
            : { type: "emoji", emoji: item }
    );

    const size = items.length > 100 ? "text-xl md:text-2xl" : "text-2xl md:text-4xl";

    const rotations = [
        "rotate-6",
        "-rotate-6",
        "rotate-12",
        "-rotate-12"
    ];

    const scales = [
        "scale-90",
        "scale-110",
        "scale-125",
    ];

    return (
        <div className="flex flex-col items-center">
            <div className="flex w-full flex-col md:flex-row items-start justify-between">
                <div>
                    <h1 className="font-bold text-neutral-500 text-center sm:text-lg">You{<>&apos;</>}ve collected items worth a{<>&nbsp;</>}total{<>&nbsp;</>}of</h1>
                    <h1 className="font-black text-gartic-dark text-xl sm:text-3xl">{data.points} points</h1>
                </div>
                <Link href={"/progress"}>
                    <Button>View your progress</Button>
                </Link>
            </div>
            <ul className="flex flex-row items-center justify-center flex-wrap mt-10">
                {items.map((item, i) => (
                    <li key={i} className={`${size} ${rotations[i % rotations.length]} ${scales[i % scales.length]} m-4 transform`}>
                        {item.type === "custom"
                            ? <Image src={item.image} width={48} height={48} alt={"item"} />
                            : <div>{item.emoji}</div>
                        }
                    </li>
                ))}
            </ul>
        </div>
    );
};

const Collection: FC = () => {
    const { isLoading, data } = useQuery<CollectionResponse>("collection", () =>
        fetch("/api/collection").then((response) => response.json())
    );

    if (isLoading) {
        return <div>Loading your collection...</div>;
    }

    return (
        <div className="flex-1 rounded-xl bg-neutral-100 p-5">
            {!data || data.points === 0
                ? <EmptyCollection />
                : <CollectionOverview data={data} />
            }
        </div>
    );
};

export default Collection;
