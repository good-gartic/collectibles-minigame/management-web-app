import type { NextApiRequest } from "next";

export const containsValidBotToken = (request: NextApiRequest): boolean => {
    const header = request.headers["authorization"];

    if (!header || !header.startsWith("Bot ")) {
        return false;
    }

    const token = process.env.API_KEY as string;
    const value = header.replace(/^Bot\s+/, "");

    return token === value;
}