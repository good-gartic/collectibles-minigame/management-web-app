
import { type NextPage } from "next";
import Head from "next/head";
import Router from "next/router";
import React from "react";
import { useSession } from "next-auth/react";
import Header from "../components/Header";
import Collection from "../components/Collection";
import Container from "../components/Container";
import Leaderboard from "../components/Leaderboard";

const Application: NextPage = () => {
    const session = useSession();

    if (session.status === "unauthenticated") {
        Router.push("/");
    }

    return (
        <>
            <Head>
                <title>Good Gartic / Collectibles</title>
                <meta name="description" content="Good Gartic's Collectibles minigame management app maintained by Hex" />
                <link rel="icon" href="/favicon.png" />
            </Head>

            <Header />

            <Container>
                <Leaderboard />
                <Collection />
            </Container>
        </>
    );
};

export default Application;
