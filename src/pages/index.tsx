import { type NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import logo from "../static/logo.png";
import { signIn, signOut, useSession } from "next-auth/react";
import React, { type FC } from "react";
import Button from "../components/Button";
import Router from "next/router";

interface UserProfileProps {
    username: string;
    avatar: string;
}

const UserProfile: FC<UserProfileProps> = ({ username, avatar }: UserProfileProps) => {
    return (
        <div className="flex flex-col items-center">
            <span className="tracking-wide uppercase text-sm font-bold text-gartic-dark">Signed in as</span>

            <div className="flex flex-row items-center mt-3 rounded-full p-4 pr-6 bg-white shadow-lg">
                <Image src={avatar} alt={`${username}'s avatar`} width={48} height={48} className="rounded-full" />
                <div className="flex flex-col items-start ml-4">
                    <h1 className="text-2xl tracking-tight font-bold">{username}</h1>
                    <button onClick={() => signOut()} className="text-amber-500 hover:text-gartic-dark font-bold">Not you? 🧐</button>
                </div>

            </div>

            <Button large onClick={() => Router.push("/app")} className="mt-20">Continue to the application</Button>
        </div>
    );
};

const Home: NextPage = () => {
    const session = useSession();

    return (
        <>
            <Head>
                <title>Good Gartic / Collectibles</title>
                <meta name="description" content="Good Gartic's Collectibles minigame management app maintained by Hex" />
                <link rel="icon" href="/favicon.png" />
            </Head>
            <main className="flex min-h-screen flex-col items-center justify-center bg-gartic-light">
                <div className="container flex flex-col items-center justify-center gap-12 px-4 py-16 ">
                    <Image src={logo} alt="Good Gartic logo" width={128} height={128} quality={100} />
                    <h1 className="mb-5 text-3xl font-extrabold tracking-tight text-gartic-dark sm:mb-10 sm:text-5xl">Collectibles<>&nbsp;</>game</h1>

                    {
                        (session.status === "authenticated")
                            ? <UserProfile username={session.data?.user?.name ?? ""} avatar={session.data?.user?.image ?? ""} />
                            : <button onClick={() => signIn("discord")} className="sm:mt-10 cursor-pointer rounded-lg bg-[#5865F2] px-8 py-4 text-xl font-black text-white">Login with Discord</button>
                    }
                </div>
            </main>
        </>
    );
};

export default Home;
