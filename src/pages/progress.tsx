import { type NextPage } from "next";
import Head from "next/head";
import CollectionProgress from "../components/CollectionProgress";
import Container from "../components/Container";
import Header from "../components/Header";

const Progress: NextPage = () => {
    return (
        <>
            <Head>
                <title>Good Gartic / Collectibles</title>
                <meta name="description" content="Good Gartic's Collectibles minigame management app maintained by Hex" />
                <link rel="icon" href="/favicon.png" />
            </Head>

            <Header />

            <Container>
                <CollectionProgress/>
            </Container>
        </>
    );
};

export default Progress;