import { type NextApiRequest, type NextApiResponse } from "next";

import { getServerAuthSession } from "../../../server/common/get-server-auth-session";
import { prisma } from "../../../server/db/client";
import { z } from "zod";

const suggestionRequest = z.object({
    emoji: z.string().length(1),
    name: z.string().min(1),
    description: z.string().min(1).optional()
})

const suggestCollection = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getServerAuthSession({ req, res });
    const validation = suggestionRequest.safeParse(req.body);

    if (req.method !== "POST" || !session || !validation.success) {
        res.status(400).send({ error: "Invalid request" });
        return;
    }

    const { data } = validation;
    const suggestion = await prisma.collectibleSuggestion.create({
        data: {
            emoji: data.emoji,
            name: data.name,
            description: data.description
        }
    });

    res.send(suggestion);
};

export default suggestCollection;
