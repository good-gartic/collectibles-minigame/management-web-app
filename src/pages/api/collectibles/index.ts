import { type NextApiRequest, type NextApiResponse } from "next";

import { prisma } from "../../../server/db/client";

const collectibles = async (req: NextApiRequest, res: NextApiResponse) => {
    res.send(await prisma.collectible.findMany());
};

export default collectibles;
