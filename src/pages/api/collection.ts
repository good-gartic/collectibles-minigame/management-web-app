import { type NextApiRequest, type NextApiResponse } from "next";

import { getServerAuthSession } from "../../server/common/get-server-auth-session";
import { prisma } from "../../server/db/client";


const collection = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getServerAuthSession({ req, res });
    const defaultScore = {
        points: 0,
        collection: ""
    };

    if (session === null) {
        res.json(defaultScore);
        return;
    }

    const score = await prisma.userScore.findFirst({
        where: {
            discordId: session?.user?.id
        }
    });

    res.status(200).json(score ?? defaultScore);
};

export default collection;
