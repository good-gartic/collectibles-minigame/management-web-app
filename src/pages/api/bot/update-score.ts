import { type NextApiRequest, type NextApiResponse } from "next";

import { containsValidBotToken } from "../../../lib/botTokenValidation";
import { prisma } from "../../../server/db/client";
import { z } from "zod";
import { type Collectible } from "@prisma/client";

const updateScoreRequest = z.object({
  discordId: z.string().min(1),
  discordName: z.string().min(1),
  discordAvatar: z.string().min(1),
  collectibleId: z.string()
});

const updateScore = async (req: NextApiRequest, res: NextApiResponse) => {
  if (!containsValidBotToken(req)) {
    res.status(403).send({ error: "Missing bot token authorization" });
    return;
  }

  const validation = updateScoreRequest.safeParse(req.body);

  if (req.method !== "POST" || !validation.success) {
    res.status(400).send({ error: "Invalid request" });
    return;
  }

  const { data } = validation;

  const collectible: Collectible = await prisma.collectible.findFirstOrThrow({
    where: {
      id: Number(data.collectibleId),
    },
  });

  const score = await prisma.userScore.upsert({
    where: {
      discordId: data.discordId,
    },
    create: {
      discordId: data.discordId,
      discordName: data.discordName,
      discordAvatar: data.discordAvatar,
      points: collectible.points,
    },
    update: {
      discordName: data.discordName,
      discordAvatar: data.discordAvatar,
      points: {
        increment: collectible.points,
      },
    },
  });

  const item = collectible.customImage !== null ? `%${collectible.customImage}%` : collectible.emoji;

  await prisma.userScore.update({
    where: {
      discordId: data.discordId,
    },
    data: {
      collection: score.collection + " " + item,
    },
  });

  res.status(201).send({ message: "User score updated" });
};

export default updateScore;
