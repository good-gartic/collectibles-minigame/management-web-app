import { type NextApiRequest, type NextApiResponse } from "next";
import type { LeaderboardResponse } from "../../types/api";

import { prisma } from "../../server/db/client";

const leaderboard = async (req: NextApiRequest, res: NextApiResponse<LeaderboardResponse>) => {
    const result = await prisma.userScore.findMany({
        select: {
            discordId: true,
            discordAvatar: true,
            discordName: true,
            points: true,
            collection: false
        },
        orderBy: {
            points: "desc"
        }
    });

    res.status(200).json(result);
};

export default leaderboard;
