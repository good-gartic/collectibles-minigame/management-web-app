import NextAuth, { type NextAuthOptions } from "next-auth";
import DiscordProvider from "next-auth/providers/discord";

import { env } from "../../../env/server.mjs";
import { prisma } from "../../../server/db/client";

export const authOptions: NextAuthOptions = {
  callbacks: {
    session: async ({ session, token }) => {
      if (session.user) {
        const id = token.sub ?? "0";
        const admin = await prisma.administrator.count({ where: { discordId: id } }) > 0;

        const user = { ...session.user, id, admin };

        return { ...session, user };
      }

      return session;
    },
    jwt: async ({ user, token }) => {
      if (user) {
        return { ...token, uid: user.id };
      }

      return token;
    },
  },
  providers: [
    DiscordProvider({
      clientId: env.DISCORD_CLIENT_ID,
      clientSecret: env.DISCORD_CLIENT_SECRET,
    }),
  ],
};

export default NextAuth(authOptions);
