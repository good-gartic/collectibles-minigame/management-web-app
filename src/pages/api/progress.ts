import { type NextApiRequest, type NextApiResponse } from "next";

import { getServerAuthSession } from "../../server/common/get-server-auth-session";
import { prisma } from "../../server/db/client";


const progress = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getServerAuthSession({ req, res });
    const collectibles = await prisma.collectible.findMany();

    const defaultResponse = {
        collection: collectibles.map(item => {
            return {
                ...item,
                collected: false
            };
        })
    };

    if (session === null) {
        res.json(defaultResponse);
        return;
    }

    const score = await prisma.userScore.findFirst({
        where: {
            discordId: session?.user?.id
        }
    });

    const collection = score?.collection ?? "";
    const response = collectibles.map(item => {
        return {
            ...item,
            collected: collection.includes(item.customImage ? `%${item.customImage}%` : item.emoji)
        };
    })

    res.status(200).json(response);
};

export default progress;
