import type { Collectible, UserScore } from "@prisma/client";

export type LeaderboardResponse = Array<Omit<UserScore, "collection">>;

export type CollectionProgressResponse = Array<Collectible & { collected: boolean}>;